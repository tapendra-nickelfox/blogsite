const express = require("express");
const authController = require("../controllers/authController");
const blogController = require("../controllers/blogController");
const likeController = require("../controllers/likeController");

const router = express.Router();

router
  .route("/")
  .get(blogController.getAllBlog)
  .post(authController.protect, blogController.createBlog);

router
  .route("/:blogId")
  .get(blogController.getBlog)
  .patch(authController.protect, blogController.updateBlog)
  .delete(authController.protect, blogController.deleteBlog);

router
  .route("/:blogId/like")
  .get(authController.protect, likeController.getBlogLikes)
  .post(authController.protect, likeController.addLike)
  .delete(authController.protect, likeController.removeLike);
module.exports = router;
