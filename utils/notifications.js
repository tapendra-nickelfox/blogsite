const dotenv = require("dotenv");
var admin = require("firebase-admin");
var serviceAccount = require("../firebase/firebase-adminsdk.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.FIREBASE_DATABASE
});

const sendNotification =(deviceToken,title,body)=>{
    const receivedToken=deviceToken;
    console.log(receivedToken)
    const messageNotification = {
      notification:
       {
         title:title,
         body: body
        },
      };
    admin.messaging().sendToDevice(receivedToken,messageNotification)
    .then(function(response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function(error) {
      console.log("Error sending message:", error);
    });
}

module.exports=sendNotification