const mongoose = require("mongoose");

const likeSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
      required: [true, "Like must belong to user"],
    },
    blog: {
      type: mongoose.Schema.ObjectId,
      ref: "Blog",
      required: [true, "Like must belong to user"],
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

likeSchema.index({ user: 1, blog: 1 }, { unique: true });
likeSchema.pre(["find", "create"], function (next) {
  this.populate({
    path: "user",
    select: "name photo",
  });
  next();
});

const Like = mongoose.model("Like", likeSchema);
module.exports = Like;
