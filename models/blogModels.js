const mongoose = require("mongoose");
const slugify = require("slugify");

const blogSchema = new mongoose.Schema(
  {
    topic: {
      type: String,
      required: [true, "Please provide topic name"],
      unique: true,
    },
    details: {
      type: String,
    },
    image: [String],
    user: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
      required: [true, "Like must belong to a user"],
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

blogSchema.pre(/^find/, function (next) {
  this.populate({
    path: "user",
    select: "name photo",
  });
  next();
});
const Blog = mongoose.model("Blog", blogSchema);
module.exports = Blog;
