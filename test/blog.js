let mongoose=require("mongodb");
let chai =require('chai');
let chaiHttp=require('chai-http');
let app=require('../server');
let should = chai.should();

chai.use(chaiHttp)

 describe('Blogs',()=>{
    //Test the get all blog
    describe('/GET BLOGS',()=>{
        it("get all blogs",async (done)=>{
         const result=   chai.request(app)
            .get('/api/v1/blogs')
            .end((err,res)=>{
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('status').eql('success')
                res.body.length.should.be.eql(2);
                res.body.data.should.have.property('blog')
                res.body.data.blog.should.be.a('array')
            done(); 
            })
        })
        
    });
    /**
     Test the single blog
     */
    describe('/GET A BLOG',()=>{
        it("get a single blog" ,(done)=>{
            chai.request(app)
            .get('/api/v1/blogs/622e5153555252cea5420eed')
            .end((err,res)=>{
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('status').eql('success')
                res.body.data.blog.should.have.property('topic')
                res.body.data.blog.should.have.property('details')
                res.body.data.blog.should.have.property('user')
               done() 
            })
        })
        
    })
    /*
    Test the /POST route
    */
    // describe('/POST Blog',()=>{
    //    it('It should post',(done)=>{
    //        let blog={
    //         topic:"faltu life",
    //         details:"Test with test3 account",
    //         images:"https://www.localhost.com"
    //        }
    //      chai.request(app)
    //         .post('/api/v1/blogs/')
    //         .set("Authorization",'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMmRkNWE5NjA5ZjY3MmI2ZjMwNDM4NCIsImlhdCI6MTY0NzIwMjM3NSwiZXhwIjoxNjU0OTc4Mzc1fQ.IaiZfoK3R4pmyqXqO_ZGk1hUAQDMh0csfb-OikamRRw')          
    //         .send(blog)
    //         .end((err,res)=>{
    //             console.log(res.body)
    //             res.should.have.status(201);
    //             res.body.should.be.a('object');
    //             res.body.should.have.property('status').eql('success')
    //             res.body.data.blog.should.have.property('topic')
    //             res.body.data.blog.should.have.property('details')
    //             res.body.data.blog.should.have.property('user')
    //             done();
    //         })

    //    })
    // })
/**
 * Test the delete blog route
 */
    // describe('/DELETE BLOG',()=>{
    //     it('It should delete blog',(done)=>{
    //         chai.request(app)
    //             .delete('/api/v1/blogs/622e593158b6d1d9391aec22')
    //             .set("Authorization",'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMmRkNWE5NjA5ZjY3MmI2ZjMwNDM4NCIsImlhdCI6MTY0NzIwMjM3NSwiZXhwIjoxNjU0OTc4Mzc1fQ.IaiZfoK3R4pmyqXqO_ZGk1hUAQDMh0csfb-OikamRRw')          
    //             .end((err,res)=>{
    //               res.should.have.status(204);
    //              done()
    //             })
    //     })
    // })
})