This RestApi allow to maintain User Account and Blogs.

## Requirements

- Postman.

* Mongodb local server or MongoDb Atlas.

## Application Account

**_You need MongoDb account and DataBase configuration to run this api _**

## Setup

**_Parameters need to be setup in config.env file_**

1. `PORT`=port number (3000).
2. `DATABASE_PASSWORD`=Database password (12345).
3. `DATABASE`=mongoDB Atlas Database path.
4. `DATABASE_LOCAL`= mongoDB local database path (mongodb://localhost:27017/blogsite).
5. `JWT_SECRET`=Secret key for JSon web token (my-secret).
6. `JWT_EXPIRES_IN`= Token expire period (90d).
7. `JWT_COOKIE_EXPIRES_IN`=Cookie expire period (90).

## To run this api

- Clone this repo in local system.
- Setup **conig.env** file.
- Run **npm i**, To install all dependencies in local system.
- Run **npm start**, To run api.
- Now test API on **Postman**.

## Working with API

This Api divides into 2 part ,User and Blogs
`Default path {URL}` = http://127.0.0.1:`{PORT}/`

### User

- `{{URL}}api/v1/users/signup` = (POST) To add new user in application.
- `{{URL}}api/v1/users/login` = (POST) Login with already existing account.
- `{{URL}}api/v1/users/me`= (GET) To view the currently logged in user profile.
- `{{URL}}api/v1/users/forgotPassword` = (POST) Enter email id of existing account and in the response user will recieve new url to reset password.
- `{{URL}}api/v1/users/resetPassword/:parameter`= (PATCH) Use the url which you have recieved in email and now reset the password with new password details.
- `{{URL}}api/v1/users/updateMyPassword`= (PATCH) To update current logged in user password.
- `{{URL}}api/v1/users/updateMe`= (PATCH) To update your profile.

### Blog

- `{{URL}}api/v1/blogs` = (POST) To add new blog
- `{{URL}}api/v1/blogs` = (GET) To view all blogs
- `{{URL}}api/v1/blogs/:blogId` = (GET) To view single blog.
- `{{URL}}api/v1/blogs/:blogId` = (Delete) To delete a blog.
- `{{URL}}api/v1/blogs/:blogId/like` = (POST) To like a blog.
- `{{URL}}api/v1/blogs/:blogId/like` = (Delete) To remove like.
- `{{URL}}api/v1/blogs/:blogId/like` = (GET) To get list of all likes on a blog.
