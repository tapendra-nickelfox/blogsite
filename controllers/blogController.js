const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const Like = require("../models/likeModels");
const Blog = require("../models/blogModels");
const sendNotification =require('../utils/notifications') 
/**
 * This controller will get all the blogs
 * @async
 * @method - [Blog] To get all blogs
 * @param {object} req -This is a req body 
 * @param {object} res - This is a res body
 * @return - Return all blogs
 */
const getAllBlogController=async (req, res) => {
  // Send notifications
  // const token="cqUTP_aEBCmcXSkygKQFaE:APA91bFDg3k1s4TiGzAetJ8JhatFtrNnFy9AQFx1H3mU7NkssdUor5DpYRN3o-Su9QVe0UFbmBYercMp5QhLTCOUtAwBISlEfPzev3sKKv15b4KpKl_jNpP4tOFhwHY3CvEHjuYEu5Qf"
  // const title="title"
  // const body="body " 
  // sendNotification(token,title,body)
  const blogs = await Blog.find();
  res.status(201).json({
    status: "success",
    length: blogs.length,
    data: {
      blog: blogs,
    },
  });
}
/**
 * This controller will add new blogs
 * @async
 * @method - [Blog] To add a blog
 * @param {object} req -This is a req body 
 * @param {object} res - This is a res body
 * @return - Return a response of new blog
 */
const createBlogController=async (req, res, next) => {
  if (!req.body.user) req.body.user = req.user.id;
  const newBlog = await Blog.create(req.body);
  res.status(201).json({
    status: "success",
    data: {
      blog: newBlog,
    },
  });
}

/**
 * This controller will get a single blogs
 * @async
 * @method - [Blog] To get a blogs
 * @param {object} req -This is a req body 
 * @param {object} res - This is a res body
 * @return - Return single blog data
 */
const getBlog=async (req, res, next) => {
  const blog = await Blog.findById(req.params.blogId);
  if (!blog) {
    return next(new AppError("No Blog find with that Id", 404));
  }
  const likes = await Like.countDocuments({ blog: req.params.blogId });
  
  res.status(201).json({
    status: "success",
    data: {
      blog,
      likes,
    },
  });
}

/**
 * This controller will update the existing blog
 * @async
 * @method - [Blog] To update a blog
 * @param {object} req -This is a req body 
 * @param {object} res - This is a res body
 * @param {function} next - next 
 * @return - Return a updated response body
 */
const updateBlogController=async (req, res, next) => {
  const blog = await Blog.findByIdAndUpdate(req.params.blogId, {
    topic: req.body.topic,
    details: req.body.details,
    image: req.body.image,
  });
  if (!blog) {
    return next(new AppError("No Blog find with that Id", 404));
  }
  res.status(201).json({
    status: "success",
    data: {
      blog,
    },
  });
}

/**
 * This controller will delete the existing blog
 * @async
 * @method - [Blog] To delete a blog
 * @param {object} req -This is a req body 
 * @param {object} res - This is a res body
 * @param {function} next - next 
 * @throws {AppError} - "Return error if failed to delete"
 * @return - Return a success 
 */
const deleteBlogController=async (req, res, next) => {
  const blog = await Blog.findById(req.params.blogId);
  if (!blog) {
    return next(new AppError("No Blog find with that Id", 404));
  }
  if (blog.user._id != req.user.id) {
    return next(new AppError("You are not the author of this blog", 404));
  }
  const blogDelete = await Blog.findByIdAndDelete(req.params.blogId);
  res.status(204).json({
    status: "success",
    data: null,
  });
}

exports.getAllBlog = catchAsync(getAllBlogController);
exports.createBlog = catchAsync(createBlogController);
exports.getBlog = catchAsync(getBlog);
exports.updateBlog = catchAsync(updateBlogController);
exports.deleteBlog = catchAsync(deleteBlogController);
