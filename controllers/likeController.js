const Like = require("../models/likeModels");
const catchAsync = require("../utils/catchAsync");

/**
 * To get all like details
 * @async
 * @method - [Like] To get all like details of blog
 * @param {object} req This is a req body
 * @param {object} res This is a res body
 * @param {function} next next call
 * @return return likes data
 */
const getBlogLikesController=async (req, res, next) => {
  const likes = await Like.find({ blog: req.params.blogId });
  res.status(201).json({
    likes,
  });
}

/**
 * Like a blog
 * @async
 * @method - [Like] To like blog
 * @param {object} req This is a req body
 * @param {object} res This is a res body
 * @param {function} next next call
 * @return return like blog info 
 */
const addLikeController=async (req, res, next) => {
  if (!req.body.user) req.body.user = req.user.id;
  req.body.blog = req.params.blogId;
  const newLike = await Like.create(req.body);
  res.status(201).json({
    status: "success",
    data: {
      like: newLike,
    },
  });
};

/**
 * To remove like
 * @async
 * @method - [Like] To remove your like
 * @param {object} req This is a req body
 * @param {object} res This is a res body
 * @param {function} next next call
 * @return return success message
 */
const removeLikeController=async (req, res, next) => {
  if (!req.body.user) req.body.user = req.user.id;
  req.body.blog = req.params.blogId;
  const liked = await Like.deleteOne({
    user: req.body.user,
    blog: req.body.blog,
  });
  console.log(liked);
  res.status(204).json({
    status: "success",
    data: null,
  });
}

exports.getBlogLikes = catchAsync(getBlogLikesController);
exports.addLike = catchAsync(addLikeController)
exports.removeLike = catchAsync(removeLikeController);
