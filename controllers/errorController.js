const AppError = require("../utils/appError");

const handleCastErrorDB = (err) => {
  const message = `Blog with this ${err.path}: ${err.value} does not exist`;
  return new AppError(message, 400);
};

const handleDuplicateFieldsDB = (err) => {
  const value = err.message.match(/(["'])(\\?.)*?\1/)[0];
  const message = `Duplicate field value: ${value}. Please use another value!`;
  return new AppError(message, 400);
};

const sendErrorProd = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    message: err.message,
    name: err.name,
  });

  // if(err.isOperational){
  //     res.status(err.statusCode).json({
  //         status: err.status,
  //         message: err.message
  //       });
  // }
  // else{
  //     res.status(500).json({
  //         status: 'error',
  //         message: 'Something went very wrong!'
  //       });
  // }
};
module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || "error";
  let error = { ...err };
  //console.log("Error sdfsdfsdfsd");
  console.log(err.name);
  if (err.name === "CastError") err = handleCastErrorDB(err);
  if (err.code === 11000) err = handleDuplicateFieldsDB(err);
  sendErrorProd(err, res);
};
