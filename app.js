const express = require("express");
const swaggerUi=require('swagger-ui-express')
const swaggerDocument=require('./docs/swagger.json')
const AppError = require("./utils/appError");
const globalErrorhandler = require("./controllers/errorController");
const cors=require('cors')
// Routes
const userRouter = require("./routes/userRoute");
const blogRouter = require("./routes/blogRoute");

const app = express();
app.use(cors())
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

app.use(express.json());
app.use((req, res, next) => {
  req.requestTime = new Date().toISOString();
  next();
});
app.use("/api/v1/users", userRouter);
app.use("/api/v1/blogs", blogRouter);
app.use(globalErrorhandler);
app.use('/api-docs',swaggerUi.serve,swaggerUi.setup(swaggerDocument));

app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});
module.exports = app;
